package com.example.grinterview.ui.base

open class BaseViewState(open val isLoading: Boolean)