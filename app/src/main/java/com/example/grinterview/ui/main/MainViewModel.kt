package com.example.grinterview.ui.main

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.MutableLiveData
import androidx.paging.*
import com.example.grinterview.api.response.User
import com.example.grinterview.common.ShowableType
import com.example.grinterview.helpers.exhaustive
import com.example.grinterview.ui.SingleLiveEvent
import com.example.grinterview.ui.base.BaseViewModel
import com.example.grinterview.ui.base.BaseViewState
import com.example.grinterview.ui.main.helpers.SimplePagingSource
import kotlinx.coroutines.CoroutineScope
import timber.log.Timber
import kotlin.coroutines.EmptyCoroutineContext

data class MainViewState(
    override val isLoading: Boolean,
    val users: PagingData<User>? = null
) : BaseViewState(isLoading = isLoading)

sealed class MainViewEvents {
    data class Navigation(val id: Int?) : MainViewEvents()
    data class Error(
        val showableType: ShowableType?,
        val withAction: Boolean = false,
        val intentAction: MainViewIntents? = null
    ) :
        MainViewEvents()
}

sealed class MainViewIntents {
    data class OnItemClicked(val id: Int) : MainViewIntents()
    data class ErrorFromPaging(val errorMsg: String, val intent: MainViewIntents) :
        MainViewIntents()

    object Init : MainViewIntents()
    object Refresh : MainViewIntents()
    data class ListLoading(val loading: Boolean) : MainViewIntents()
    data class EndOfListInfo(val message: String) : MainViewIntents()
}

class MainViewModel @ViewModelInject constructor(
    private val simplePagingSource: SimplePagingSource
) : BaseViewModel<MainViewState, MainViewEvents, MainViewIntents>() {

    override fun initState(): MainViewState = MainViewState(true)

    private val navigateToDetail: SingleLiveEvent<Int> = SingleLiveEvent()
    private val pager = Pager(
        PagingConfig(5, prefetchDistance = 1),
        pagingSourceFactory = { simplePagingSource.provideSource() }
    )

    private val eventsData: SingleLiveEvent<MainViewEvents> = SingleLiveEvent()

    private val mutableUsersList: MutableLiveData<PagingData<User>> = MutableLiveData()
    private val loadingState: MutableLiveData<Boolean> = MutableLiveData()

    init {
        reduceSourceToState()
    }

    private fun reduceSourceToState() {
        mutableUsersList.addAsSource { (data, previousState) ->
            previousState.copy(users = data)
        }

        loadingState.addAsSource { (loading, previousState) ->
            previousState.copy(isLoading = loading)
        }

        addSingleEventSource(eventsData)

        addSingleEventSource(navigateToDetail) {
            MainViewEvents.Navigation(it)
        }
    }

    override fun processIntent(viewIntent: MainViewIntents) {
        when (viewIntent) {
            MainViewIntents.Init -> {
                loadingState.value = true
                pager.liveData
                    .cachedIn(CoroutineScope(EmptyCoroutineContext)) //fixing issue with IllegalStateException cannot collect twice from pager
                    .addAsSource { (data, previousState) ->
                        previousState.copy(
                            users = data,
                            isLoading = false
                        )
                    }
            }
            is MainViewIntents.OnItemClicked -> {
                navigateToDetail.postValue(viewIntent.id)
            }
            MainViewIntents.Refresh -> {
                Timber.d("Refreshing")
            }
            is MainViewIntents.ErrorFromPaging -> {
                eventsData.postValue(
                    MainViewEvents.Error(
                        showableType = ShowableType.Error(
                            viewIntent.errorMsg
                        ),
                        withAction = true,
                        intentAction = viewIntent.intent
                    )
                )
            }
            is MainViewIntents.ListLoading -> loadingState.postValue(viewIntent.loading)
            is MainViewIntents.EndOfListInfo -> eventsData.postValue(
                MainViewEvents.Error(
                    ShowableType.Info(viewIntent.message)
                )
            )
        }.exhaustive
    }
}