package com.example.grinterview.ui.detail

import android.os.Bundle
import android.widget.ImageView
import coil.api.load
import coil.request.CachePolicy
import coil.transform.CircleCropTransformation
import com.example.grinterview.R
import com.example.grinterview.common.show
import com.example.grinterview.helpers.show
import com.example.grinterview.ui.base.BaseFragment
import com.hannesdorfmann.fragmentargs.annotation.Arg
import com.hannesdorfmann.fragmentargs.annotation.FragmentWithArgs
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.fragment_detail.*

@AndroidEntryPoint
@FragmentWithArgs
class DetailFragment :
    BaseFragment<DetailViewState, DetailViewEvents, DetailViewIntents, DetailViewModel>() {

    @Arg(required = true)
    var userId: Int = -1

    override fun getViewModelClass(): Class<DetailViewModel> = DetailViewModel::class.java

    override fun getLayoutResId(): Int = R.layout.fragment_detail

    override fun init(savedInstanceState: Bundle?) {
        DetailFragmentBuilder.injectArguments(this)
        process(DetailViewIntents.Init)

        if (userId != -1) {
            process(DetailViewIntents.UserId(userId))
        }

        sv_refresh_details.setOnRefreshListener {
            process(DetailViewIntents.UserId(userId))
        }
    }

    override fun renderViewEvents(viewEvent: DetailViewEvents?) {
        when (viewEvent) {
            is DetailViewEvents.Error -> {
                viewEvent.showableType.show(baseActivity, true, "Retry") {
                    process(viewEvent.intent)
                }
                sv_refresh_details.isRefreshing = false
            }
            null -> {
            }
        }
    }

    override fun render(viewState: DetailViewState) {
        viewState.userDetails?.data?.avatar?.let {
            loadAvatar(it, iv_person_thumbnail)
        }

        viewState.userDetails?.let {
            sv_refresh_details.isRefreshing = false
            tv_mail.text = it.data.email
            tv_name.text = "${it.data.first_name} ${it.data.last_name}"
            tv_desc.text = it.ad.text
            tv_company.text = it.ad.company
            tv_url.text = it.ad.url
        }

        pb_detail_background.show(viewState.initState)
        user_card.show(!viewState.initState)

        pb_detail_page.show(viewState.isLoading)
    }

    private fun loadAvatar(url: String, view: ImageView) {
        view.load(url) {
            placeholder(R.drawable.github_placeholder)
            transformations(CircleCropTransformation())
            crossfade(200)
            diskCachePolicy(CachePolicy.ENABLED)
        }
    }
}