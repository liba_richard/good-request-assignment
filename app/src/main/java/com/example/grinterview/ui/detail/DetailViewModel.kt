package com.example.grinterview.ui.detail

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.MutableLiveData
import com.example.grinterview.api.PublicApi
import com.example.grinterview.api.response.User
import com.example.grinterview.api.response.UserDetails
import com.example.grinterview.common.ShowableType
import com.example.grinterview.ui.base.BaseViewModel
import com.example.grinterview.ui.base.BaseViewState
import io.reactivex.schedulers.Schedulers
import timber.log.Timber

data class DetailViewState(
    val initState: Boolean = true,
    val userDetails: UserDetails? = null,
    override val isLoading: Boolean = false
) : BaseViewState(isLoading = isLoading)

sealed class DetailViewEvents {
    data class Error(val showableType: ShowableType?, val intent: DetailViewIntents) :
        DetailViewEvents()
}

sealed class DetailViewIntents {
    data class UserId(val userId: Int) : DetailViewIntents()
    object Init : DetailViewIntents()
}

class DetailViewModel @ViewModelInject constructor(
    private val baseApi: PublicApi
) : BaseViewModel<DetailViewState, DetailViewEvents, DetailViewIntents>() {

    private val loading: MutableLiveData<Boolean> = MutableLiveData()
    private val userDetails: MutableLiveData<UserDetails> = MutableLiveData()
    private val initialised: MutableLiveData<Unit> = MutableLiveData()


    init {
        reduceSourceToState()
    }

    private fun reduceSourceToState() {
        userDetails.addAsSource { (data, previousState) ->
            previousState.copy(userDetails = data, isLoading = false)
        }

        loading.addAsSource { (loading, previousState) ->
            previousState.copy(isLoading = loading)
        }

        initialised.addAsSource {
            it.second.copy(initState = false)
        }
    }

    override fun initState(): DetailViewState = DetailViewState()

    override fun processIntent(viewIntent: DetailViewIntents) {
        when (viewIntent) {
            DetailViewIntents.Init -> {
                initialised.postValue(Unit)
            }
            is DetailViewIntents.UserId -> {
                baseApi.getUserDetail(viewIntent.userId)
                    .subscribeOn(Schedulers.io())
                    .doOnSubscribe {
                        loading.postValue(true)
                    }
                    .map {
                        userDetails.postValue(it)
                        Timber.d("$it")
                    }
                    .onErrorReturn {
                        loading.postValue(false)
                        it.message?.let {
                            viewEvents.postValue(
                                DetailViewEvents.Error(
                                    ShowableType.Error(
                                        "General error loading data."
                                    ),
                                    viewIntent
                                )
                            )
                        }
                    }
                    .subscribe()
            }
        }
    }
}