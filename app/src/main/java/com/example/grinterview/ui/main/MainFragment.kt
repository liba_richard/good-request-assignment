package com.example.grinterview.ui.main

import android.os.Bundle
import androidx.lifecycle.lifecycleScope
import androidx.paging.LoadState
import androidx.recyclerview.widget.ConcatAdapter
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.grinterview.R
import com.example.grinterview.common.show
import com.example.grinterview.helpers.exhaustive
import com.example.grinterview.helpers.show
import com.example.grinterview.ui.base.BaseFragment
import com.example.grinterview.ui.detail.DetailFragmentBuilder
import com.example.grinterview.ui.main.helpers.LoadNewPageStateAdapter
import com.example.grinterview.ui.main.helpers.UserAdapter
import com.hannesdorfmann.fragmentargs.annotation.FragmentWithArgs
import dagger.hilt.android.AndroidEntryPoint
import io.reactivex.subjects.PublishSubject
import kotlinx.android.synthetic.main.fragment_main.*
import kotlinx.coroutines.launch
import timber.log.Timber
import java.net.UnknownHostException
import java.util.concurrent.TimeUnit


@AndroidEntryPoint
@FragmentWithArgs
class MainFragment :
    BaseFragment<MainViewState, MainViewEvents, MainViewIntents, MainViewModel>() {

    private val onItemClickListener: PublishSubject<Int> = PublishSubject.create()
    private lateinit var adapter: ConcatAdapter
    private lateinit var usersAdapter: UserAdapter

    override fun getLayoutResId() = R.layout.fragment_main

    override fun getViewModelClass(): Class<MainViewModel> = MainViewModel::class.java

    override fun init(savedInstanceState: Bundle?) {
        MainFragmentBuilder.injectArguments(this)
        usersAdapter = UserAdapter(onItemClickListener)
        adapter = usersAdapter.withLoadStateFooter(
            footer = LoadNewPageStateAdapter()
        )

        recycler_view.apply {
            layoutManager = LinearLayoutManager(requireContext())
            adapter = this@MainFragment.adapter
        }

        refresh_layout.setOnRefreshListener {
            process(MainViewIntents.Refresh)
            usersAdapter.refresh()
        }

        if (savedInstanceState == null) {
            process(MainViewIntents.Init)
        }

        usersAdapter.addLoadStateListener { loadStates ->
            Timber.d("loadStates : $loadStates")
//            if (loadStates.append.endOfPaginationReached) {
//                process(MainViewIntents.EndOfListInfo("End of Pagination reached"))
//            }
            loadStates.refresh.let {
                when (it) {
                    is LoadState.NotLoading -> {
                        process(MainViewIntents.ListLoading(false))
                    }
                    LoadState.Loading -> {
                        process(MainViewIntents.ListLoading(true))
                    }
                    is LoadState.Error -> {
                        process(MainViewIntents.ListLoading(false))
                        if (it.error is UnknownHostException) {
                            process(
                                MainViewIntents.ErrorFromPaging(
                                    "No Internet connection.",
                                    MainViewIntents.Refresh
                                )
                            )
                        } else {
                            process(
                                MainViewIntents.ErrorFromPaging(
                                    "General error",
                                    MainViewIntents.Refresh
                                )
                            )
                        }
                    }
                }
            }
        }

        onItemClickListener
            .throttleFirst(300, TimeUnit.MILLISECONDS)
            .subscribe(
                { process(MainViewIntents.OnItemClicked(it)) },
                {}).addDisposable()
    }

    override fun renderViewEvents(viewEvent: MainViewEvents?) {
        when (viewEvent) {
            is MainViewEvents.Navigation -> {
                viewEvent.id?.let {
                    baseActivity?.changeTo(
                        DetailFragmentBuilder(it).build(),
                        withBack = true,
                        replace = false
                    )
                }
            }
            is MainViewEvents.Error -> {
                if (viewEvent.withAction) {
                    viewEvent.showableType.show(baseActivity, true, "Retry") {
                        viewEvent.intentAction?.let {
                            process(it)
                        }
                    }
                } else {
                    viewEvent.showableType.show(baseActivity)
                }
            }
            null -> {
            }
        }.exhaustive
    }

    override fun render(viewState: MainViewState) {
        viewState.users?.let {
            lifecycleScope.launch {
                refresh_layout.isRefreshing = false
                usersAdapter.submitData(it)
            }
        }
        pb_main_page.show(viewState.isLoading)
    }
}