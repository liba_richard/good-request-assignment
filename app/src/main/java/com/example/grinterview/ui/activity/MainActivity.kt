package com.example.grinterview.ui.activity

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import com.example.grinterview.R
import com.example.grinterview.ui.main.MainFragmentBuilder
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.main_activity.*


@AndroidEntryPoint
class MainActivity : BaseActivity() {

    override fun getRootLayout(): View? = cl_root

    override fun getLayoutResId(): Int = R.layout.main_activity

    override fun init(savedInstanceState: Bundle?) {
        if (savedInstanceState == null) {
            changeTo(MainFragmentBuilder().build(), false)
        }
    }

    private fun getContentId(): Int = R.id.container

    override fun changeTo(fragment: Fragment, withBack: Boolean, replace: Boolean) {
        val fragmentTransaction = supportFragmentManager.beginTransaction()

        if (withBack) {
            if (replace) {
                fragmentTransaction.replace(getContentId(), fragment, fragment.javaClass.simpleName)
            } else {
                fragmentTransaction.add(getContentId(), fragment, fragment.javaClass.simpleName)
            }
            fragmentTransaction.addToBackStack(fragment.javaClass.simpleName)
        } else {
            fragmentTransaction.replace(getContentId(), fragment, fragment.javaClass.simpleName)
        }
        fragmentTransaction.commitAllowingStateLoss()
    }

}