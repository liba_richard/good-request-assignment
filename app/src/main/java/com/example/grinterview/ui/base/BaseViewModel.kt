package com.example.grinterview.ui.base

import android.os.Handler
import androidx.annotation.CallSuper
import androidx.annotation.MainThread
import androidx.annotation.NonNull
import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.grinterview.ui.SingleLiveEvent
import timber.log.Timber

abstract class BaseViewModel<VS : BaseViewState, VE, VI> :
    ViewModel() {
    private val _viewEvents: SingleLiveEvent<VE> = SingleLiveEvent()
    protected val mediatorViewState: MediatorLiveData<VS> = MediatorLiveData()

    val viewState: LiveData<VS> = mediatorViewState
    val viewEvents: SingleLiveEvent<VE> = _viewEvents
    val mediatorViewEvents: MediatorLiveData<VE> = MediatorLiveData()

    init {
        initialize()
    }

    abstract fun initState(): VS

    private fun initialize() {
        mediatorViewState.postValue(initState())
    }

    @MainThread
    private fun <S, T> MediatorLiveData<T>.addSourceSafely(
        @NonNull source: LiveData<S>,
        @NonNull onChanged: (S) -> Unit
    ) {
        try {
            this.addSource(source, onChanged)
        } catch (e: IllegalArgumentException) {
            Timber.e(e)
        }
    }

    fun <T> MutableLiveData<T>.addAsSource(@NonNull onChanged: (Pair<T, VS>) -> VS) {
        mediatorViewState.addSourceSafely(this) {
            mediatorViewState.value = onChanged(Pair(it, viewState.value ?: initState()))
        }
    }

    fun <T> LiveData<T>.addAsSource(@NonNull onChanged: (Pair<T, VS>) -> VS) {
        mediatorViewState.addSourceSafely(this) {
            mediatorViewState.value = onChanged(Pair(it, viewState.value ?: initState()))
        }
    }

    fun addSingleEventSource(@NonNull source: SingleLiveEvent<VE>) {
        mediatorViewEvents.addSourceSafely(source) {
            it?.let {
                _viewEvents.value = it
            }
        }
    }

    fun <S> addSource(
        @NonNull source: MutableLiveData<S?>,
        @NonNull onChanged: (Pair<S?, VS>) -> VS
    ) {
        if (source is SingleLiveEvent<S>) {
            throw RuntimeException("Source added into view state merge logic not view Events: $source \n Use addSingleEventSource function")
        } else {
            source.addAsSource(onChanged)
        }
    }

    fun <T> addSingleEventSource(
        @NonNull source: SingleLiveEvent<T>,
        @NonNull onChanged: (T?) -> VE?
    ) {
        mediatorViewEvents.addSourceSafely(source) {
            _viewEvents.value = onChanged(it)
        }
    }


    @CallSuper
    open fun process(viewIntent: VI) {
        // dirty hack 2 because of base + subclass initialization order
        Handler().post {
            Timber.d("processing view intent = $viewIntent")
            processIntent(viewIntent)
        }
    }

    abstract fun processIntent(viewIntent: VI)
}