package com.example.grinterview.ui.main.helpers

import androidx.paging.ExperimentalPagingApi
import androidx.paging.PagingState
import androidx.paging.rxjava2.RxPagingSource
import com.example.grinterview.api.PublicApi
import com.example.grinterview.api.response.User
import io.reactivex.Single
import io.reactivex.schedulers.Schedulers
import java.util.concurrent.TimeUnit
import javax.inject.Inject

class SimplePagingSource @Inject constructor(
    private val api: PublicApi
) {

    @OptIn(ExperimentalPagingApi::class)
    fun provideSource(): RxPagingSource<Int, User> {
        return object : RxPagingSource<Int, User>() {

            override fun getRefreshKey(state: PagingState<Int, User>): Int? {
                return state.anchorPosition ?: return null
            }

            override fun loadSingle(params: LoadParams<Int>): Single<LoadResult<Int, User>> {
                val pageToLoad = params.key ?: 1
                val nextPage = pageToLoad + 1
                return api.getNewUsersPaged(pageToLoad)
                    .subscribeOn(Schedulers.io())
                    // delay for loading new page state to be more visible
                    .delay(150, TimeUnit.MILLISECONDS)
                    .map { response ->
                        LoadResult.Page(
                            response.data,
                            null,
                            if (nextPage > response.total_pages) null else nextPage,
                            LoadResult.Page.COUNT_UNDEFINED,
                            LoadResult.Page.COUNT_UNDEFINED
                        ) as LoadResult<Int, User>
                    }.onErrorReturn { LoadResult.Error(it) }
            }
        }
    }
}