package com.example.grinterview.ui.base

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.*
import com.example.grinterview.ui.activity.BaseActivity
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import timber.log.Timber

abstract class BaseFragment<VS : BaseViewState, VE, VI, VM : BaseViewModel<VS, VE, VI>> :
    Fragment() {

    protected val viewModel: VM by lazy { ViewModelProvider(this).get(getViewModelClass()) }
    protected abstract fun getViewModelClass(): Class<VM>

    abstract fun getLayoutResId(): Int

    private var compositeDisposable: CompositeDisposable? = null

    fun addToDisposable(disposable: Disposable) {
        getCompositeDisposable().add(disposable)
    }

    var baseActivity: BaseActivity? = activity as? BaseActivity

    override fun onAttach(context: Context) {
        super.onAttach(context)
        baseActivity = activity as? BaseActivity
    }

    fun Disposable.addDisposable() {
        getCompositeDisposable().add(this)
    }

    override fun onDestroy() {
        dispose()
        super.onDestroy()
    }

    fun dispose() {
        getCompositeDisposable().dispose()
    }

    protected fun getCompositeDisposable(): CompositeDisposable {
        return compositeDisposable?.let {
            if (it.isDisposed) {
                compositeDisposable = CompositeDisposable()
                compositeDisposable
            } else {
                it
            }
        } ?: CompositeDisposable().also { compositeDisposable = it }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        return inflater.inflate(getLayoutResId(), container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        preInit()
        init(savedInstanceState)
    }

    private fun preInit() {
        viewModel.viewState.observe(viewLifecycleOwner, Observer { viewState ->
            Timber.i("View state = $viewState")

            render(viewState)
        })

        viewModel.viewEvents.observe(viewLifecycleOwner, Observer { viewEvent ->
            Timber.i("View event = $viewEvent")
            renderViewEvents(viewEvent)
        })

        viewModel.mediatorViewEvents.observe(viewLifecycleOwner, Observer { })
    }

    abstract fun init(savedInstanceState: Bundle?)

    abstract fun renderViewEvents(viewEvent: VE?)

    abstract fun render(viewState: VS)

    protected fun process(viewIntent: VI) {
        viewModel.process(viewIntent)
    }
}


