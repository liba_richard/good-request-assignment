package com.example.grinterview.ui.main.helpers

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.paging.PagingDataAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import coil.api.load
import coil.request.CachePolicy
import coil.transform.CircleCropTransformation
import com.example.grinterview.R
import com.example.grinterview.api.response.User
import io.reactivex.subjects.PublishSubject
import kotlinx.android.synthetic.main.item_layout.view.*

internal class UserAdapter(
    val onItemClickListener: PublishSubject<Int>,
    diffCallback: DiffUtil.ItemCallback<User> = object : DiffUtil.ItemCallback<User>() {
        override fun areItemsTheSame(oldItem: User, newItem: User): Boolean =
            oldItem.id == newItem.id

        override fun areContentsTheSame(oldItem: User, newItem: User): Boolean = oldItem == newItem
    }
) : PagingDataAdapter<User, UserAdapter.UserViewHolder>(diffCallback) {

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): UserViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_layout, parent, false)

        return UserViewHolder(view)
    }

    override fun onBindViewHolder(
        holder: UserViewHolder,
        position: Int
    ) {
        val item = getItem(position)
        holder.bind(item)
    }

    inner class UserViewHolder(parent: View) : RecyclerView.ViewHolder(parent) {
        fun bind(user: User?) {
            if (user != null) {
                itemView.apply {
                    setOnClickListener { onItemClickListener.onNext(user.id) }
                    loadAvatar(user.avatar, this.iv_person_thumbnail)
                    tv_name.text = "${user.first_name} ${user.last_name}"
                    tv_mail.text = user.email
                }
            }
        }

        private fun loadAvatar(url: String, view: ImageView) {
            view.load(url) {
                placeholder(R.drawable.github_placeholder)
                error(R.drawable.github_placeholder)
                transformations(CircleCropTransformation())
                crossfade(200)
                diskCachePolicy(CachePolicy.ENABLED)
            }
        }
    }
}