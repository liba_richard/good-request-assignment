package com.example.grinterview.api.response

data class UserDetails(
    val data: UserDetail,
    val ad: Ad
)

data class UserDetail(
    val id: Int,
    val email: String,
    val first_name: String,
    val last_name: String,
    val avatar: String
)

data class Ad(
    val company: String,
    val url: String,
    val text: String
)