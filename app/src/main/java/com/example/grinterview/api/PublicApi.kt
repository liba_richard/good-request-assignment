package com.example.grinterview.api

import com.example.grinterview.api.response.UserDetails
import com.example.grinterview.api.response.Users
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface PublicApi {

    @GET("users/{userId}")
    fun getUserDetail(@Path("userId") userId: Int): Single<UserDetails>

    @GET("users")
    fun getNewUsersPaged(
        @Query("page") pageNum: Int,
        @Query("per_page") perPage: Int = 5
    ): Single<Users>

}