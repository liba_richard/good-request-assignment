package com.example.grinterview.di

import android.content.Context
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ApplicationComponent
import dagger.hilt.android.qualifiers.ApplicationContext

@Module
@InstallIn(ApplicationComponent::class)
class ApplicationContextModule {

    @Provides
    fun provideContext(@ApplicationContext context: Context): Context = context

}