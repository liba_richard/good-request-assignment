package com.example.grinterview.helpers

val <T> T.exhaustive: T
    get() = this