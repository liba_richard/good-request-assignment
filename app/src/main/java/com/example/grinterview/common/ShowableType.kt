package com.example.grinterview.common

import android.widget.Button
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import com.example.grinterview.R
import com.example.grinterview.helpers.exhaustive
import com.example.grinterview.ui.activity.BaseActivity
import com.shasin.notificationbanner.Banner
import java.io.Serializable


sealed class ShowableType : Serializable {
    abstract val message: String

    data class Error(override val message: String) : ShowableType()
    data class Info(override val message: String) : ShowableType()
    data class Success(override val message: String) : ShowableType()
}

fun ShowableType?.show(
    baseActivity: BaseActivity?,
    withAction: Boolean = false,
    btnText: String = "",
    onBtnAction: () -> Unit = {}
) {
    if (this == null || baseActivity == null) return
    when (this) {
        is ShowableType.Error -> {
            if (withAction) {
                buildBannerWithButton(baseActivity, onBtnAction, btnText)
            } else {
                buildBanner(baseActivity, Banner.ERROR)
            }
        }
        is ShowableType.Info -> {
            buildBanner(baseActivity, Banner.INFO)
        }
        is ShowableType.Success -> {
            buildBanner(baseActivity, Banner.SUCCESS)
        }
    }.exhaustive
}

private fun ShowableType.buildBannerWithButton(
    baseActivity: BaseActivity,
    onBtnAction: () -> Unit,
    btnText: String
) {
    baseActivity.let { activity ->
        activity.getRootLayout()?.let { rootView ->
            val banner = Banner.make(rootView, activity, R.layout.banner_with_retry, false)

            val textView = banner.bannerView.findViewById<TextView>(R.id.status_text)
            val root = banner.bannerView.findViewById<ConstraintLayout>(R.id.cl_banner_root)
            val btn = banner.bannerView.findViewById<Button>(R.id.btn_banner_action)
            textView.text = this.message

            btn.text = btnText
            btn.setOnClickListener {
                onBtnAction()
                banner.dismissBanner()
            }

            root.setOnClickListener {
                banner.dismissBanner()
            }
            banner.show()
        }
    }
}

private fun ShowableType.buildBanner(baseActivity: BaseActivity, bannerType: Int) {
    baseActivity.let { activity ->
        activity.getRootLayout()?.let { rootView ->
            Banner.make(
                rootView,
                activity,
                bannerType,
                this.message,
                Banner.TOP,
                5_000
            ).apply {
                bannerView.setOnClickListener { dismissBanner() }
                show()
            }
        }
    }
}